﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlitchAdd : MonoBehaviour
{
    GlitchEffect glitchEffect;
    private void OnEnable()
    {
        if (glitchEffect == null) glitchEffect = FindObjectOfType<GlitchEffect>();
        glitchEffect.enabled = true;
    }
    private void OnDisable()
    {
        if (glitchEffect == null) glitchEffect = FindObjectOfType<GlitchEffect>();
        glitchEffect.enabled = false;
    }
}
