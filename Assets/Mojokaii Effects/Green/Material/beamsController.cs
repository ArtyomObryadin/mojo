﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class beamsController : MonoBehaviour
{
    [SerializeField] private float angle;
    float time;
    float startAngle;
    float currAngle;
    void Start()
    {
        StartCoroutine("ColorCahnge");
        StartCoroutine("AngleChange");
        startAngle = angle;
        currAngle = 0;
    }
    private IEnumerator ColorCahnge()
    {
        yield return new WaitForSeconds(4.3f);
        gameObject.GetComponent<ParticleSystem>().startColor = Color.green;
    }
    private IEnumerator AngleChange()
    {
        yield return new WaitForSeconds(4.8f);
        time = 0;
        angle = 0;
        currAngle = startAngle;
    }
    void Update()
    {
        time += Time.deltaTime;
        gameObject.transform.localRotation = Quaternion.Euler(0,Mathf.Lerp(currAngle, angle, time/2),0);
    }
}
