﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class EqualizerController : MonoBehaviour
{
    private void OnEnable()
    {
        GameObject.FindGameObjectWithTag("Equalizer").GetComponent<VideoPlayer>().enabled = true;
        GameObject.FindGameObjectWithTag("Equalizer").GetComponent<MeshRenderer>().enabled = true;
    }
    private void OnDisable()
    {
        //GameObject.FindGameObjectWithTag("Equalizer").GetComponent<MeshRenderer>().enabled = false;
        //GameObject.FindGameObjectWithTag("Equalizer").GetComponent<VideoPlayer>().enabled = false;
    }
}
