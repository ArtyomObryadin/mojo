﻿using UnityEngine;
using System.Collections;
using Wear.ARCompanyCore.Managers;

public class skeletonMusik : MonoBehaviour
{
    public GameObject prefabBar;
    int scaler = 64;//
    public float width = 0.001f;
    public bool isRaindow;
    bool isRaise = true;
    bool isG = true, isB;
    float B = 0, G = 0;
    int numSamples = 64;
    int rateUpdate = 4;
    float[] number = new float[64];
    GameObject[] bars;
    float spacing;
    Color color = new Color(1, 0, 0);

    void Start()
    {
        spacing = width * 2;
        AudioListener Audio = Camera.main.GetComponent<AudioListener>();
        bars = new GameObject[numSamples];
        for (int i = 0; i < numSamples; i++)
        {
            float xpos = i * spacing - width * 64;
            bars[i] = (GameObject)Instantiate(prefabBar, gameObject.transform) as GameObject;
            bars[i].transform.localPosition = new Vector3(xpos, 0, 0);
        }
    }
    void Update()
    {
        if (isRaindow)
        {
            if ((B < 1 || G < 1) && isRaise)
            {
                if (isG)
                {
                    G += 0.01f;
                }
                else
                {
                    B += 0.01f;
                }
                if (G >= 1 || B >= 1)
                {
                    isRaise = false;
                }
            }
            else if ((B > 0 || G > 0) && !isRaise)
            {
                if (isG)
                {
                    G -= 0.01f;
                }
                else
                {
                    B -= 0.01f;
                }
                if (G < 0 || B < 0)
                {
                    isRaise = true;
                    G = 0;
                    B = 0;
                }

            }
            color = new Color(G, 1, B);
        }
        number = AudioListener.GetSpectrumData(numSamples, 0, FFTWindow.BlackmanHarris);
        if (rateUpdate == 4)
        {
            for (int i = 0; i < numSamples; i++)
            {
                if (float.IsInfinity(number[i]) || float.IsNaN(number[i]))
                {
                }
                else
                {
                    float currElem;
                    if (scaler / 2 - i >= 0)
                    {
                        currElem = number[scaler / 2 - i];
                        if (number[scaler / 2 - i] > prefabBar.transform.localScale.z * 5)
                        {
                            currElem = number[scaler / 2 - i] / 2;
                        }
                        bars[i].transform.localScale = new Vector3(prefabBar.transform.localScale.x, prefabBar.transform.localScale.y, Mathf.Clamp(currElem, 0, prefabBar.transform.localScale.z * 5 ));
                    }
                    else
                    {
                        currElem = number[i - scaler / 2];
                        if (number[i - scaler / 2] > prefabBar.transform.localScale.z * 5)
                        {
                            currElem = number[i - scaler / 2] / 2;
                        }
                        bars[i].transform.localScale = new Vector3(prefabBar.transform.localScale.x, prefabBar.transform.localScale.y, Mathf.Clamp(currElem, 0, prefabBar.transform.localScale.z * 5));
                    }
                }
                if (isRaindow)
                {
                    bars[i].GetComponent<Renderer>().material.color = color;
                }
            }
            rateUpdate = 0;
        }
        rateUpdate++;
    }
}