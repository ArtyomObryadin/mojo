using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TextureCompressor : MonoBehaviour {

   // [MenuItem("Assets/Compressor/CopyFromAndroidToiOS")]
    static void CopyAndroidToiOS() {
        
        TextureImporterPlatformSettings tiPs = new TextureImporterPlatformSettings();
   
        foreach (var selected in Selection.GetFiltered(typeof(Object), SelectionMode.Assets)) {
            TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(AssetDatabase.GetAssetPath(selected));
            importer.GetPlatformTextureSettings("Android").CopyTo(tiPs);
            Debug.Log(tiPs.name);
            tiPs.name = "iPhone";
            tiPs.overridden = true;
            Debug.Log(tiPs.maxTextureSize);

            importer.SetPlatformTextureSettings(tiPs);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(selected));
        }
    }

  //  [MenuItem("Assets/Compressor/CopyFromiOSToAndroid")]
    static void CopyiOStoAndroid() {
        TextureImporterPlatformSettings tiPs = new TextureImporterPlatformSettings();

        foreach (var selected in Selection.GetFiltered(typeof(Object), SelectionMode.Assets)) {
            if (selected is Object) {
                TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(AssetDatabase.GetAssetPath(selected));
                importer.GetPlatformTextureSettings("iPhone").CopyTo(tiPs);
                Debug.Log(tiPs.name);
                tiPs.name = "Android";
                tiPs.overridden = true;
                Debug.Log(tiPs.maxTextureSize);

                importer.SetPlatformTextureSettings(tiPs);
                AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(selected));
            }
        }

    }

    public static void FromAndroidToiOSCompression(List<string> fileLinks) {

        TextureImporterPlatformSettings tiPs = new TextureImporterPlatformSettings();

        foreach (var file in fileLinks) {
            if (file != "") {
                TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(file);
                importer.GetPlatformTextureSettings("Android").CopyTo(tiPs);
                Debug.Log(tiPs.name);
                tiPs.name = "iPhone";
                tiPs.overridden = true;
                Debug.Log(tiPs.maxTextureSize);

                importer.SetPlatformTextureSettings(tiPs);
                importer.SaveAndReimport();
                // AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(file));
            }
        }
    }

    public static void FromiOSToAndroidCompression(List<string> fileLinks) {

        TextureImporterPlatformSettings tiPs = new TextureImporterPlatformSettings();

        foreach (var file in fileLinks) {
            if (file != "") {
                TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(file);
                importer.GetPlatformTextureSettings("iPhone").CopyTo(tiPs);
                Debug.Log(tiPs.name);
                tiPs.name = "Android";
                tiPs.overridden = true;
                Debug.Log(tiPs.maxTextureSize);

                importer.SetPlatformTextureSettings(tiPs);
               // AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(file));
            }
        }
    }
}
