﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MiniJSON;

namespace Wear.ARCompanyCore.Utils {
    public class MainUtils {
        public static void JsonParse<T>(string json, ref List<T> obj) where T : new() {
            if (!string.IsNullOrEmpty(json)) {
                IDictionary peepDataSearch = (IDictionary)Json.Deserialize(json);

                if (peepDataSearch["items"] != null) {
                    Debug.Log(Json.Serialize(peepDataSearch["items"]));
                    IList categoriesList = (IList)Json.Deserialize(Json.Serialize(peepDataSearch["items"]));

                    foreach (var elem in categoriesList) {
                        T objElem = new T()/*default*/;
                        JsonUtility.FromJsonOverwrite(Json.Serialize(elem), objElem);
                        obj.Add(objElem);
                    }

                    Debug.LogFormat("<color=#00ff00ff> Peeps was loaded sucsessfull!! </color>");
                }
                else {
                    Debug.LogError("Music Track not contains a data!");
                }
            }
        }

        public static void CheckError(string error) {
            Debug.Log("Attention - " + error);
            if (error.Contains("400")) {
                //CachedAndUpdateProfile();
            }
            else if (error.Contains("404")) {
                Debug.LogError("Server Error (404)");
            }
            else if (error.Contains("Cannot resolve destination host")) {
                //Managers.UIManager.InternetOnLoadErrorPopUp();
                Debug.LogFormat("<color=#ff0000ff> Cannot resolve destination host </color>");
            }
            else if (error.Contains("Unknown Error")) {
                //Managers.UIManager.InternetOnLoadErrorPopUp();
                Debug.LogFormat("<color=#ff0000ff> Unknown Error!! Attention!! </color>");
            }
        }
    }
}
