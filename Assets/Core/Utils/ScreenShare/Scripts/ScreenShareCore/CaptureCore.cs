﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using NatSuite.Recorders;
using NatSuite.Recorders.Clocks;
using NatSuite.Recorders.Inputs;
using UniRx.Async;
using UnityEngine;

namespace Wear.Utils.CaptureAndShare
{
    public class CaptureCore : MonoBehaviour {
        public const int CodeVersion = 102;
        public static bool VersionChecked;

        public delegate void VoidCallback();
        public delegate void StringCallback(string result);
        public delegate void BoolCallback(bool result);

        public static VoidCallback OnScreenshotCapturingStart;
        public static VoidCallback OnScreenshotCapturingEnd;
        public static VoidCallback OnVideoCapturingStart;
        public static VoidCallback OnVideoCapturingEnd;

        private static CaptureSettings.SettingsData _mainSettings;
        private static CaptureCore _mainCapture;

        #region CaptureCore params

        private Camera _targetCamera;
        private AudioListener _targetListener;
        private MP4Recorder _videoRecorder;
        private IClock _recordingClock;

        private CameraInput _cameraInput;
        private AudioInput _audioInputMain;
        private AudioSource _microphoneAudioSource;
        private AudioClip _micAudioClip;

        private string _tempImageURL;
        private Texture2D _tempImage;
        private string _tempVideoURL;
        private bool destroyWhenCallbackInvoked;

        private bool _lastContentIsVideo;
        private bool _videoRecording;
        private string _lastContentRecordTemp;
        private static int _maxMicFreq;

        #endregion

        #region Mono Logic

        void OnDestroy() {
            DisposeTemp();
        }

        private void SetCamera(Camera param) {
            _targetCamera = param;
        }

        private void SetAudioListener(AudioListener param) {
            _targetListener = param;
        }

        private void Kill() {
            if (_videoRecording) {
                EndVideoRecord();
            }

            Destroy(gameObject);
        }

        void DisposeTemp() {
            StopAllCoroutines();

            if (File.Exists(_tempImageURL)) {
                File.Delete(_tempImageURL);
            }

            if (_tempImage != null) {
                _tempImage = null;
            }
        }

        public Texture2D GetTempScreenshot() {
            if (_tempImage != null) {
                return _tempImage;
            }
            else {
                return null;
            }
        }

        /// <summary>
        /// Create temp screenshot
        /// </summary>
        /// <param name="tempCallback">Callback, return tempMedia path</param>
        public void CreateTempScreenshot(StringCallback tempCallback = null) {
            DisposeTemp();
            StartCoroutine(coroutine_CreateTempScreenshot(tempCallback));
        }

        /// <summary>
        /// [Process] Create temp screenshot
        /// </summary>
        /// <param name="tempCallback">Callback, return tempMedia path</param>
        /// <returns></returns>
        IEnumerator coroutine_CreateTempScreenshot(StringCallback tempCallback = null) {
            OnScreenshotCapturingStart?.Invoke();

            yield return new WaitForEndOfFrame();
            //Prepare
            _tempImage = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);

            //Capture, encode to byte's
            _tempImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            _tempImage.Apply();
            byte[] imageBytes = _tempImage.EncodeToPNG();

            //Get temp directory
            _tempImageURL = Path.Combine(Application.persistentDataPath, _mainSettings.tempMediaPath);

            //Check path and final prepare path
            if (!Directory.Exists(_tempImageURL)) {
                Directory.CreateDirectory(_tempImageURL);
            }
            //Get temp screenshot path 
            _tempImageURL = Path.Combine(_tempImageURL, $"TEMP_{_mainSettings.FileNamePrefix}_{System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss")}.png");
            _lastContentRecordTemp = _tempImageURL;
            _lastContentIsVideo = false;

            //Save TEMP
            File.WriteAllBytes(_tempImageURL, imageBytes);

            tempCallback?.Invoke(_tempImageURL);
            OnScreenshotCapturingEnd?.Invoke();

            if (destroyWhenCallbackInvoked) {
                Kill();
            }
        }

        public static void CheckVersions() {
            if (!VersionChecked) {
                VersionChecked = true;
                Debug.Log("[CaptureCore] VERSION CHECKER");
                int currentVersion = (int)(CodeVersion / 100f);
                Debug.Log("[CaptureCore] [CaptureCore] [" + (currentVersion == 1 ? "Correct version" : "ANOTHER version") + "] Version: " + (CodeVersion / 100f));
                Debug.Log("[CaptureCore] [CaptureControl] [" + (currentVersion == (int)(CaptureControl.CodeVersion / 100f) ? "Correct version" : "<color=red>ANOTHER version</color>") + "] Version: " + (CaptureControl.CodeVersion / 100f));
                Debug.Log("[CaptureCore] [CaptureSettings] [" + (currentVersion == (int)(CaptureSettings.CodeVersion / 100f) ? "Correct version" : "<color=red>ANOTHER version</color>") + "] Version: " + (CaptureSettings.CodeVersion / 100f));
                Debug.Log("[CaptureCore] [HideWhenCapture] [" + (currentVersion == (int)(HideWhenCapture.CodeVersion / 100f) ? "Correct version" : "<color=red>ANOTHER version</color>") + "] Version: " + (HideWhenCapture.CodeVersion / 100f));
                Debug.Log("[CaptureCore] [SaveShareManager] [" + (currentVersion == (int)(SaveShareManager.CodeVersion / 100f) ? "Correct version" : "<color=red>ANOTHER version</color>") + "] Version: " + (SaveShareManager.CodeVersion / 100f));
                //Debug.Log("[CaptureCore] [CameraInput] [" + (currentVersion == (int)(CameraInput.CodeVersion / 100f) ? "Correct version" : "<color=red>ANOTHER version</color>") + "] Version: " + (CameraInput.CodeVersion / 100f));
                //Debug.Log("[CaptureCore] [AudioInput] [" + (currentVersion == (int)(AudioInput.CodeVersion / 100f) ? "Correct version" : "<color=red>ANOTHER version</color>") + "] Version: " + (AudioInput.CodeVersion / 100f));
                //Debug.Log("[CaptureCore] [MP4Recorder] [" + (currentVersion == (int)(MP4Recorder.CodeVersion / 100f) ? "Correct version" : "<color=red>ANOTHER version</color>") + "] Version: " + (MP4Recorder.CodeVersion / 100f));
                //Debug.Log("[CaptureCore] [RealtimeClock] [" + (currentVersion == (int)(RealtimeClock.CodeVersion / 100f) ? "Correct version" : "<color=red>ANOTHER version</color>") + "] Version: " + (RealtimeClock.CodeVersion / 100f));
            }
        }

        public async Task StartCaptureTempVideo() {
            _videoRecording = true;
            await StartMicrophoneRecord();

            DisposeTemp();

            OnVideoCapturingStart?.Invoke();

            _recordingClock = new RealtimeClock();
            _videoRecorder = new MP4Recorder(
                _mainSettings.GetRecordResolution().x,
                _mainSettings.GetRecordResolution().y,
                _mainSettings.framerateParam,
                AudioSettings.outputSampleRate,
                (int)AudioSettings.speakerMode,
                ((int)(960 * 540 * 11.4f)),
                _mainSettings.keyframes
            );

            var cameras = new List<Camera>();
            cameras.AddRange(Camera.allCameras.Where(c=>c.tag!="Glitch"));                                   

            // Create recording inputs
            _cameraInput = new CameraInput(_videoRecorder, _recordingClock/*,_mainSettings.writeFrameCounter*/, cameras.ToArray());

            if (_mainSettings.audioRecordMode != CaptureSettings.AudioRecordMode.WithoutAudio) {
                if (_mainSettings.audioRecordMode == CaptureSettings.AudioRecordMode.Microphone) {

                    _audioInputMain = new AudioInput(_videoRecorder, _recordingClock, _microphoneAudioSource, true);
                }
                else {
                    _audioInputMain = new AudioInput(_videoRecorder, _recordingClock, _targetListener);
                }
            }
        }

        private async Task StartMicrophoneRecord() {
            if (_microphoneAudioSource == null) {
                _microphoneAudioSource = _targetCamera.GetComponent<AudioSource>();
                if (_microphoneAudioSource == null) {
                    _microphoneAudioSource = _targetCamera.gameObject.AddComponent<AudioSource>();
                }
            }

            // Code for low-latency microphone input taken from
            // https://support.unity3d.com/hc/en-us/articles/206485253-How-do-I-get-Unity-to-playback-a-Microphone-input-in-real-time-
            // It seems that there is still a latency of more than 200ms, which is too much for real-time processing.
            _micAudioClip = Microphone.Start(null, true, 30, _maxMicFreq/*_mainSettings.SampleRate*/);

            await UniTask.WaitWhile(() => !(Microphone.GetPosition(null) > 0));

            // Configure audio playback
            _microphoneAudioSource.clip = _micAudioClip;
            _microphoneAudioSource.loop = true;
            _microphoneAudioSource.Play();
            //iPhoneSpeaker.ForceToSpeaker();
        }

        void StopMicrophoneRecord() {
            Microphone.End(_mainSettings.MicDevice);
            _microphoneAudioSource.Stop();
            _microphoneAudioSource.clip = null;
        }

        public async Task EndCaptureVideo() {
            if (_mainSettings.audioRecordMode != CaptureSettings.AudioRecordMode.WithoutAudio) {
                if (_mainSettings.audioRecordMode == CaptureSettings.AudioRecordMode.Microphone) {
                    StopMicrophoneRecord();
                }

                _audioInputMain.Dispose();
            }

            _cameraInput.Dispose();
            //_videoRecorder.Dispose();
            string path = await _videoRecorder.FinishWriting(); // My changes
            VideoTempRecordingCallback(path);

            _videoRecording = false;
            ClearPreviousVideos(path);
        }

        void ClearPreviousVideos(string path) {
            string[] fileEntries = Directory.GetFiles(Path.GetDirectoryName(path));

            foreach (var elem in fileEntries) {
                if (Path.GetExtension(elem) == ".mp4" && !elem.Contains(path)) {
                    File.Delete(elem);
                    Debug.Log(elem);
                }
            }
        }

        void VideoTempRecordingCallback(string path) {
            _tempVideoURL = path;
            _lastContentRecordTemp = path;
            _lastContentIsVideo = true;
            OnVideoCapturingEnd?.Invoke();
        }

        #endregion

        #region Static Logic

        public static string GetLastContentPass() {
            if (_mainCapture == null) {
                Debug.LogException(new Exception("[CaptureCore] 'CaptureCoreInstance' is null! Please, before use FrameCapture, create capture instance via 'SetCapture'."));
                return "";
            }

            return _mainCapture._lastContentRecordTemp;
        }

        private static void CreateCapture(Camera mainCamera, bool DestroyWhenCallbackInvoked) {
            KillRecorder();

            var captureProcessor = new GameObject("CAPTURE_CORE_INSTANCE").AddComponent<CaptureCore>();
            captureProcessor.SetCamera(mainCamera);
            captureProcessor.destroyWhenCallbackInvoked = DestroyWhenCallbackInvoked;
            _mainCapture = captureProcessor;

            if (_mainSettings == null) {
                Debug.Log("[CaptureCore] '_mainSettings' is null, load default!");
                _mainSettings = CaptureSettings.GetDefault().mainData;
            }
        }

        private static void CreateCapture(Camera mainCamera, AudioListener audioListener, bool DestroyWhenCallbackInvoked)
        {
            KillRecorder();

            var captureProcessor = new GameObject("CAPTURE_CORE_INSTANCE").AddComponent<CaptureCore>();
            captureProcessor.SetCamera(mainCamera);
            captureProcessor.SetAudioListener(audioListener);
            captureProcessor.destroyWhenCallbackInvoked = DestroyWhenCallbackInvoked;
            _mainCapture = captureProcessor;

            if (_mainSettings == null) {
                Debug.Log("[CaptureCore] '_mainSettings' is null, load default!");
                _mainSettings = CaptureSettings.GetDefault().mainData;
            }
        }

        #endregion

        public static void SetCapture(Camera mainCamera, AudioListener mainListener, int micMaxFreq, bool DestroyWhenCallbackInvoked = false)
        {

            if (mainCamera == null) {
                Debug.Log("[CaptureCore] 'mainCamera' and is null!");
                mainCamera = Camera.main;

                if (mainCamera == null) {
                    Debug.LogException(new Exception("[CaptureCore] 'mainCamera' and 'Camera.main' is null! Capture can't work without camera! \n Check params for null or availability camera on scene."));
                    return;
                }
            }

            if (mainListener == null) {
                Debug.Log("[CaptureCore] 'mainListener' is null!");
                if (mainCamera != null) {
                    mainListener = mainCamera.GetComponent<AudioListener>();
                    if (mainListener == null) {
                        Debug.Log("[CaptureCore] 'mainListener' is null!");
                    }
                }

                mainListener = GameObject.FindObjectOfType<AudioListener>();
                if (mainListener == null) {
                    Debug.Log("[CaptureCore] 'mainListener' is null! Recorder work without audio!");
                }
            }

            _maxMicFreq = micMaxFreq != 0 ? micMaxFreq : 44100;

            if (mainListener == null) {
                CreateCapture(mainCamera, DestroyWhenCallbackInvoked);
            }
            else {
                CreateCapture(mainCamera, mainListener, DestroyWhenCallbackInvoked);
            }


        }

        /// <summary>
        /// Create temp screenshot
        /// </summary>
        /// <param name="callback">Callback, return tempMedia path</param>
        public static void FrameCapture(StringCallback callback = null) {
            if (_mainCapture == null) {
                Debug.LogException(new Exception("[CaptureCore] 'CaptureCoreInstance' is null! Please, before use FrameCapture, create capture instance via 'SetCapture'."));
                return;
            }

            _mainCapture.CreateTempScreenshot(callback);
        }

        public static void StartVideoRecord() {
            if (_mainCapture == null) {
                Debug.LogException(new Exception("[CaptureCore] 'CaptureCoreInstance' is null! Please, before use FrameCapture, create capture instance via 'SetCapture'."));
                return;
            }

            _mainCapture.StartCaptureTempVideo();
        }

        public static void EndVideoRecord() {
            if (_mainCapture == null) {
                Debug.LogException(new Exception("[CaptureCore] 'CaptureCoreInstance' is null! Please, before use FrameCapture, create capture instance via 'SetCapture'."));
                return;
            }

            _mainCapture.EndCaptureVideo();
        }

        public static void KillRecorder() {
            if (_mainCapture != null) {
                _mainCapture.Kill();
            }
        }

        public static CaptureCore GetCaptureCoreInstance() {
            if (_mainCapture != null) {
                return _mainCapture;
            }
            else {
                Debug.LogException(new Exception("[CaptureCore] 'CaptureCoreInstance' is null! Please, before get CaptureCore, create it via 'SetCapture'."));
                return null;
            }
        }

        public static bool LastContentIsVideo() {
            if (_mainCapture != null) {
                return _mainCapture._lastContentIsVideo;
            }
            else {
                Debug.LogException(new Exception("[CaptureCore] 'CaptureCoreInstance' is null! Please, before get CaptureCore, create it via 'SetCapture'."));
                return false;
            }
        }

        public static bool VideoNowRecording() {
            if (_mainCapture != null) {
                return _mainCapture._videoRecording;
            }
            else {
                Debug.LogException(new Exception("[CaptureCore] 'CaptureCoreInstance' is null! Please, before get CaptureCore, create it via 'SetCapture'."));
                return false;
            }
        }

        public static bool CaptureCoreInstanced() {
            return _mainCapture != null;
        }

        public static void SetSettings(CaptureSettings set) {
            if (set == null) {
                _mainSettings = CaptureSettings.GetDefault().mainData;
            }
            else {
                _mainSettings = set.mainData;
            }

        }
    }
}
