﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wear.Utils.CaptureAndShare
{
    public class CaptureControl : MonoBehaviour {
        public static int CodeVersion = 102;

        public class TempContentHolder {
            public enum tempDataType {
                videoPath, screenshotTexture2D
            }

            public tempDataType dataType;
            public string videoPath;
            public Texture2D screenshotTexture2D;

            public bool IsVideoTemp() {
                return dataType == tempDataType.videoPath;
            }
        }

        [SerializeField] private Camera _mainCamera;
        [SerializeField] private CaptureSettings _mainCaptureSettings;
        [SerializeField] private bool _clearTempWhenSave = false;
        [SerializeField] private bool _clearTempWhenShare = false;
        [SerializeField] private CaptureCore core;

        public event Action<string> OnScreenshotCaptured;
        public event Action OnScreenshotSaved;
        public event Action OnScreenshotShared;

        public event Action OnVideoStartCapture;
        public event Action<string> OnVideoEndCapture;
        public event Action OnVideoSaved;
        public event Action OnVideoShared;

        private int minFreq, maxFreq = 0; 

        private void Start() {
            //iPhoneSpeaker.ForceToSpeaker();
            StartCoroutine(MicrophoneInit());
        }

        private void Awake() {
            CaptureCore.CheckVersions();
            CaptureCore.SetSettings(_mainCaptureSettings);
        }

        [ContextMenu("Make Screenshot")]
        public void MakeScreenshot() {
            if (!CaptureCore.CaptureCoreInstanced() || CaptureCore.LastContentIsVideo()) {  
                CaptureCore.SetCapture(_mainCamera == null ? Camera.main : _mainCamera, null, maxFreq, false);
            }

            CaptureCore.FrameCapture(FrameCaptureCallback);
        }

        [ContextMenu("Media Content Share")]
        public void MediaShare() {
            if (CaptureCore.LastContentIsVideo()) {
                VideoShareCallback(CaptureCore.GetLastContentPass());
            }
            else {
                ScreenshotShareCallback(CaptureCore.GetLastContentPass());
            }

            Debug.Log(CaptureCore.GetLastContentPass());
            SaveShareManager.ShareContent(CaptureCore.GetLastContentPass());

            if (_clearTempWhenShare) {
                CaptureCore.KillRecorder();
            }
        }

        [ContextMenu("Media Content Save")]
        public void MediaSave() {
            if (CaptureCore.LastContentIsVideo()) {
                SaveShareManager.SaveVideo(CaptureCore.GetLastContentPass(),
                    _mainCaptureSettings.mainData.AlbumName,
                    "Video_0",
                    VideoSaveCallback);
            }
            else {
                SaveShareManager.SavePhoto(CaptureCore.GetCaptureCoreInstance().GetTempScreenshot(),
                    _mainCaptureSettings.mainData.AlbumName,
                    "Photo_0",
                    ScreenshotSaveCallback);
            }

            if (_clearTempWhenSave) {
                CaptureCore.KillRecorder();
            }
        }

        [ContextMenu("Clear This Temp")]
        public void ClearThisTemp() {
            CaptureCore.KillRecorder();
        }

        [ContextMenu("Start Capture Video")]
        public void StartCaptureVideo() {
            if (!CaptureCore.CaptureCoreInstanced() || !CaptureCore.LastContentIsVideo()) {
                CaptureCore.SetCapture(_mainCamera == null ? Camera.main : _mainCamera, null, maxFreq, false);
            }

            CaptureCore.StartVideoRecord();
        }

        [ContextMenu("Stop Capture Video")]
        public void StopCaptureVideo() {
            OnVideoStartCapture?.Invoke();

            if (!CaptureCore.CaptureCoreInstanced() || !CaptureCore.VideoNowRecording()) {
                Debug.LogException(new Exception("[CaptureControl] Before stop video record, please start video record"));
                return;
            }

            CaptureCore.EndVideoRecord();

            OnVideoEndCapture?.Invoke(CaptureCore.GetLastContentPass());
        }

        /// <summary>
        /// Get data for preview or replay content
        /// </summary>
        /// <returns></returns>
        public TempContentHolder GetTempData() {
            TempContentHolder result = new TempContentHolder();

            if (CaptureCore.CaptureCoreInstanced()) {
                if (CaptureCore.LastContentIsVideo()) {
                    result.dataType = TempContentHolder.tempDataType.videoPath;
                    result.videoPath = GetTempVideoPath();
                }
                else {
                    result.dataType = TempContentHolder.tempDataType.screenshotTexture2D;
                    result.screenshotTexture2D = GetScreenshotTexture2D();
                }

                return result;
            }
            else {
                Debug.LogException(new Exception("[CaptureControl] Before get ScreenshotTexture - call MakeScreenshot() and wait OnScreenshotCaptured event!"));
                return null;
            }
        }

        public Texture2D GetScreenshotTexture2D() {
            if (!CaptureCore.CaptureCoreInstanced()) {
                Debug.LogException(new Exception("[CaptureControl] Before get ScreenshotTexture - call MakeScreenshot() and wait OnScreenshotCaptured event!"));
                return null;
            }

            if (CaptureCore.LastContentIsVideo()) {
                Debug.LogException(new Exception("[CaptureControl] In this moment exist only video. In temp can exist only one type of content!"));
                return null;
            }

            return CaptureCore.GetCaptureCoreInstance().GetTempScreenshot();
        }

        public string GetTempVideoPath() {
            if (!CaptureCore.CaptureCoreInstanced()) {
                Debug.LogException(new Exception("[CaptureControl] CaptureCore not exist. Please wait OnVideoEndCapture event!"));
                return null;
            }

            if (!CaptureCore.LastContentIsVideo()) {
                Debug.LogException(new Exception("[CaptureControl] In this moment exist only screenshot. In temp can exist only one type of content!"));
                return null;
            }

            return CaptureCore.GetLastContentPass();
        }

        private void FrameCaptureCallback(string path) {
            OnScreenshotCaptured?.Invoke(path);

            UnityEngine.Debug.Log("TEMP SAVED " + path);
        }

        private void ScreenshotShareCallback(string status) {
            OnScreenshotShared?.Invoke();

            UnityEngine.Debug.Log("Try screenshot share: " + status);
        }

        private void VideoShareCallback(string status) {
            OnVideoShared?.Invoke();

            UnityEngine.Debug.Log("Try video share: " + status);
        }

        private void ScreenshotSaveCallback(string status) {
            OnScreenshotSaved?.Invoke();

            UnityEngine.Debug.Log("Screenshot save to gallery status: " + status);
        }

        private void VideoSaveCallback(string status) {
            OnVideoSaved?.Invoke();

            UnityEngine.Debug.Log("Video save to gallery status: " + status);
        }

        IEnumerator MicrophoneInit() {
#if UNITY_IOS
            yield return Application.RequestUserAuthorization(UserAuthorization.Microphone); //OR DOWN IN BLOCK
            if (Application.HasUserAuthorization(UserAuthorization.Microphone)) {
                Debug.Log("Microphone found");
            } else {
                Debug.Log("Microphone not found");
            }
#endif
            yield return new WaitUntil(() => Microphone.devices.Length > 0);

            foreach (var device in Microphone.devices) {
                string deviceName = device;
                Microphone.GetDeviceCaps(deviceName, out minFreq, out maxFreq);
                Debug.LogFormat("<color=#00ff00ff> Name: " + device + "</color>");
                Debug.LogFormat("<color=#00ff00ff> Ferquency: MIN - " + minFreq + " MAX - " + maxFreq + "</color>");
            }
        }
    }
}
