﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using MiniJSON;
using Wear.ARCompanyCore.Managers;

namespace Wear.ARCompanyCore.Utils {
    public class WebRequestStatus : Exception {
        public string RawErrorMessage { get; private set; }
        public bool HasResponse { get; private set; }
        public string Text { get; private set; }
        public System.Net.HttpStatusCode StatusCode { get; private set; }
        public System.Collections.Generic.Dictionary<string, string> ResponseHeaders { get; private set; }
        public UnityWebRequest WWW { get; private set; }
        public bool isError { get; private set; }

        public WebRequestStatus(UnityWebRequest www)
        {
            this.WWW = www;
            this.RawErrorMessage = www.error;
            this.ResponseHeaders = www.GetResponseHeaders();
            this.HasResponse = false;

            //.LogFormat("<color=#00ff00ff> =====WebRequestStatus====== " + www.uri + "</color>");

            int statusCode;
            if (int.TryParse(www.responseCode.ToString(), out statusCode)) {
                this.HasResponse = true;
                this.StatusCode = (System.Net.HttpStatusCode)statusCode;
                //Debug.Log(statusCode);
            }

            if (www.isHttpError || www.isNetworkError || !string.IsNullOrEmpty(www.error)) {// || statusCode == 404) {

                Debug.LogFormat("<color=#ff0000ff> HTTPErr - " + www.isHttpError + " NetError - " + www.isNetworkError + " www.error " + www.error + "</color>");
                if (www.isNetworkError) {
                    //MiniPeeps_Managers.Managers.UIManager.InternetOnLoadErrorPopUp();
                    Manager.UIManager.ShowNoInternetPopUp();
                    Debug.LogFormat("<color=#ff0000ff> Cannot resolve destination host </color>");
                }
                //else if (www.isHttpError && MiniPeeps_Managers.Managers.AppManager.IsSkipReg) {
                    //Debug.Log("+++++++++++++++++++++++++++++++++++++++++++HTTPError for uri " + www.uri);
                //}
                else if (www.isHttpError && !www.uri.ToString().Contains("UpdateDeviceFirebaseToken")) {
                    //MiniPeeps_Managers.Managers.UIManager.ServerErrorPopUp();
                }

                isError = true;
            }
        }

        // cache the text because if www was disposed, can't access it.
        public WebRequestStatus(UnityWebRequest www, string text)
        {
            this.WWW = www;
            this.RawErrorMessage = www.error;
            this.ResponseHeaders = www.GetResponseHeaders();
            this.HasResponse = false;
            this.Text = text;

            int statusCode;
            if (int.TryParse(www.responseCode.ToString(), out statusCode)) {
                this.HasResponse = true;
                this.StatusCode = (System.Net.HttpStatusCode)statusCode;
                Debug.Log(statusCode);
            }
            if (www.isHttpError || www.isNetworkError || !string.IsNullOrEmpty(www.error)) {// || statusCode == 404) {
                isError = true;
            }
        }

        public override string ToString()
        {
            var text = this.Text;
            if (string.IsNullOrEmpty(text)) {
                return RawErrorMessage;
            }
            else {
                return RawErrorMessage + " " + text;
            }
        }
    }

    public static class ExtensionMethods {
        // Deep clone
        public static T DeepClone<T>(this T a)
        {
            using (MemoryStream stream = new MemoryStream()) {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, a);
                stream.Position = 0;
                return (T)formatter.Deserialize(stream);
            }
        }

        public static Dictionary<K, V> ToDictionary<K, V>(this Hashtable table)
        {
            return table
              .Cast<DictionaryEntry>()
              .ToDictionary(kvp => (K)kvp.Key, kvp => (V)kvp.Value);
        }

        public static bool IsSuccessfull(this UnityWebRequest request)
        {
            Debug.LogFormat("<color=#00ff00ff> IsSuccessfull - " + new WebRequestStatus(request).isError + "</color>");
            return !new WebRequestStatus(request).isError;
        }

        public static void JsonParse<T>(string json, ref List<T> obj) where T : new()
        {
            if (!string.IsNullOrEmpty(json)) {
                IDictionary peepDataSearch = (IDictionary)Json.Deserialize(json);

                if (peepDataSearch["data"] != null) {
                    Debug.Log(Json.Serialize(peepDataSearch["data"]));
                    IList categoriesList = (IList)Json.Deserialize(Json.Serialize(peepDataSearch["data"]));

                    foreach (var elem in categoriesList) {
                        T objElem = new T()/*default*/;
                        JsonUtility.FromJsonOverwrite(Json.Serialize(elem), objElem);
                        obj.Add(objElem);
                    }

                    Debug.LogFormat("<color=#00ff00ff> Peeps was loaded sucsessfull!! </color>");
                }
                else {
                    Debug.LogError("Music Track not contains a data!");
                }
            }
        }

        public static void JsonParse<T>(string json, ref T deserObj)
        {
            if (!string.IsNullOrEmpty(json)) {
                IDictionary peepDataSearch = (IDictionary)Json.Deserialize(json);

                if (peepDataSearch["data"] != null) {

                    Debug.Log(Json.Serialize(peepDataSearch["data"]));
                    JsonUtility.FromJsonOverwrite(Json.Serialize(peepDataSearch["data"]), deserObj);

                    Debug.LogFormat("<color=#00ff00ff> Peeps was loaded sucsessfull!! " + typeof(T).Name + "</color>");
                }
                else {
                    //deserObj = null;
                    Debug.LogError("Peeps not contains a data!");
                }
            }
        }

        public static Texture2D ScaleTexture(Texture2D source, int targetWidth, int targetHeight)
        {
            Texture2D result = new Texture2D(targetWidth, targetHeight, source.format, true);
            Color[] rpixels = result.GetPixels(0);
            float incX = ((float)1 / source.width) * ((float)source.width / targetWidth);
            float incY = ((float)1 / source.height) * ((float)source.height / targetHeight);
            for (int px = 0; px < rpixels.Length; px++) {
                rpixels[px] = source.GetPixelBilinear(incX * ((float)px % targetWidth),
                                  incY * ((float)Mathf.Floor(px / targetWidth)));
            }
            result.SetPixels(rpixels, 0);
            result.Apply();
            return result;
        }
    }
}
