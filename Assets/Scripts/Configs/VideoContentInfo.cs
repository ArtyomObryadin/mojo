﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

namespace Wear.ARCompanyCore {
    //[CreateAssetMenu(fileName = "VideoInfoSO", menuName = "Wear/Core/Content/Video Info")]
    public class VideoContentInfo {//: ScriptableObject {
        public string name;
        public string videoPath;
        public bool loop;
        public List<Effect> effects;

        public VideoContentInfo() { }

        public VideoContentInfo(VideoData data) {
            name = data.name;
            loop = data.loop;

            foreach (var elem in data.timeline) {
                effects.Add(new Effect(elem));
            }
        }
    }
}
