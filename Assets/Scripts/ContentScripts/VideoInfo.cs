﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;
using UniRx.Async;

using Wear.ARCompanyCore.Utils;
using Wear.ARCompanyCore.Managers;

namespace Wear.ARCompanyCore {
    public class VideoInfo : IContent, ICacheable {

        List<VideoData> currentContent;

        public List<VideoData> CurrentContent { get => currentContent; set => currentContent = value; }
        public List<VideoData> CachedContent { get; set; }
        public ContentType contentType { get; set; }

        CacheInfo _cacheInfo;

        public string CacheId { get => Manager.CacheManager.cacheConfig.VideoInfoName; }
        public CacheInfo CacheInfo {
            get {
                if (_cacheInfo != null) {
                    return _cacheInfo;
                }
                else {
                    return _cacheInfo = new CacheInfo() {
                        Name = CacheId,
                        LastModifiedData = DateTime.Now.ToString(),
                        CachePath = ""
                    };
                };
            }

            set { _cacheInfo = value; }
        }

        public VideoInfo() {
            Init();
        }

        public async UniTask Init() {
            contentType = ContentType.Video;
            await GetAllContentList();
            await UniTask.WaitUntil(() => CurrentContent != null && CurrentContent.Count > 0);

            CachedContent = CacheManager.GetObjectByType<List<VideoData>>(CacheInfo);
            Debug.Log("CachedContent Count -------> " + CachedContent.Count);
            RevalidateVideoContent(Manager.ContentManager.IsLoadAllContent);
        }

        public async UniTask GetAllContentList() {
            string json = await ApiManager.GetVideosList();
            currentContent = new List<VideoData>();

            Utils.MainUtils.JsonParse(json, ref currentContent);
        }

        private async void RevalidateVideoContent(bool isLoadAllContent) {    
            List<string> needReload = new List<string>();
            List<bool> isContentAddedList = new List<bool>();
            if (CachedContent != null) {
                if (!isLoadAllContent) {
                    foreach (var elemCach in CachedContent) {
                        foreach (var elemBack in CurrentContent) {
                            if (elemCach.id == elemBack.id) {
                                if (elemCach.updated_at != elemBack.updated_at) {
                                    needReload.Add(elemCach.name);
                                }

                                break;
                            }
                        }
                    }
                }
                else {
                    foreach (var elemBack in CurrentContent) {
                        bool isNeedReload = true;
                        foreach (var elemCach in CachedContent) {
                            if (elemCach.id == elemBack.id) {
                                if (elemCach.updated_at == elemBack.updated_at) {
                                    isNeedReload = false;
                                }

                                break;
                            }
                        }

                        if (isNeedReload) {
                            needReload.Add(elemBack.name);
                        }
                    }
                }
            }
            else {
                if (isLoadAllContent && CurrentContent != null) {
                    foreach (var elem in CurrentContent) {
                        needReload.Add(elem.name);
                    }
                }
            }

            Manager.UIManager.DownLoadPanelController(true);
            foreach (var elem in needReload) { //Need Loader
                isContentAddedList.Add(await GetContentPath(elem));        
            }
            Manager.UIManager.DownLoadPanelController(false);

            if (isContentAddedList.Any(n => n)) {
                CacheManager.CacheObject(CachedContent, CacheInfo);
            }

            Manager.ContentManager.IsLoadAllContent = false;            
        }

        public async UniTask GetConcreteContentPath(string name) {

            VideoData curentData = null;
            bool isExistInCache = false;
            bool isAddInCache = false;

            for (int i = 0; i < CachedContent?.Count; i++) {
                if (CachedContent[i].name == name) {
                    isExistInCache = true;
                    break;
                }
            }

            if (!isExistInCache) {
                isAddInCache = await GetContentPath(name);
            }

            if (!isAddInCache && !isExistInCache) {
                Debug.LogError("Video file with name \"" + name + "\" doesn't exist");   //Add pop up message
            }
            else { 
                CacheManager.CacheObject(CachedContent, CacheInfo);
            }

            return;
        }

        public async UniTask<bool> GetContentPath(string name) {
            VideoData currentElem = null;
            bool isContentInCache = false;

            await UniTask.WaitUntil(() => CurrentContent?.Count > 0);

            foreach (var elem in CurrentContent) {
                if (name == elem.name) {
                    currentElem = elem;
                    break;
                }
            }

            if (/*!isContentInCache &&*/ currentElem != null) {
                isContentInCache = await GetConcreteContent(currentElem);
            }

            return isContentInCache;
        }

        async UniTask<bool> GetConcreteContent(ICacheable contentObj)  { 
            VideoData currentElem = (VideoData)contentObj;
            List<string> effectNames = new List<string>();

            if (currentElem != null) {
                string fileName = currentElem.CacheInfo.Name;
                CacheManager.CheckFileByInfoAndDelete(currentElem.CacheInfo);

                bool isError = true;

                await ApiManager.GetContentFromServer(currentElem.video_url, currentElem.CacheInfo, (x) => { isError = !x.WWW.IsSuccessfull(); Debug.Log(x.Text); MainUtils.CheckError(x.Text); });

                if (!isError) {
                    if (CachedContent?.Count > 0) {
                        for (int i = 0; i < CachedContent.Count; i++) {
                            if (currentElem.id == CachedContent[i].id) {
                                CachedContent[i] = currentElem;
                                return true;
                            }
                        }
                    }

                    CachedContent.Add(currentElem);

                    //Manager.ContentManager.GetOrientation(currentElem.orientation);

                    if (!Manager.ContentManager.IsLoadAllContent) {
                        for (int i = 0; i < currentElem.timeline?.Length; i++) {
                            effectNames.Add(currentElem.timeline[i].effect);
                        }

                        await Manager.ContentManager.EffectsContentInfo.GetEffectsAndCache(effectNames);
                    }

                    return true;
                }
            }
            else {
                Debug.LogError("Video with name " + currentElem.name + " doesn't exist on BackEnd side");
            }

            return false;
        }
    }
}
