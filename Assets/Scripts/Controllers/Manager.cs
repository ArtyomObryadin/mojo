﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wear.ARCompanyCore.Managers {
    public class Manager : MonoBehaviour {

        static Manager instance;

        public static UIManager UIManager;
        public static CacheManager CacheManager;
        public static ContentManager ContentManager;
        public static ApiManager ApiManager;
        public static EffectController EffectController;

        public static Manager Instance { get { return instance; } }

        void Awake() {
            if (instance == null) { instance = GetComponent<Manager>(); }
            UIManager = GetComponentInChildren<UIManager>();
            CacheManager = GetComponentInChildren<CacheManager>();
            ContentManager = GetComponentInChildren<ContentManager>();
            ApiManager = GetComponentInChildren<ApiManager>();
            EffectController = GetComponentInChildren<EffectController>();
            CacheManager.Init(Application.persistentDataPath + "/Cache/");
        }
    }
}
