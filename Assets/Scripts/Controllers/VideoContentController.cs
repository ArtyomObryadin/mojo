﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using Wikitude;
using Wear.ARCompanyCore.Managers;

namespace Wear.ARCompanyCore {
    public class VideoContentController : MonoBehaviour, IContentController {

        [SerializeField]
        VideoPlayer videoPlayer;
        [SerializeField]
        GameObject planeObj;

        private string targetName;
        private ImageTrackable parentObj;
        private List<GameObject> effectObjs;
        private VideoData currentVideoData;

        public event System.Action OnVideoStatusChange = () => { };
        public event System.Action OnVideoEndIsReached = () => { };

        public double ClipLength { get { return videoPlayer.length; } }
        public double CurrentClipTime { get { return videoPlayer.time; } }

        void Start() { }

        private void OnEnable() {
            transform.localScale = Vector3.one;
            videoPlayer.Prepare();
            videoPlayer.loopPointReached += AfterTheEnd;
            videoPlayer.prepareCompleted += VideoIsPrepared;

            effectObjs = new List<GameObject>();
        }

        private void OnDisable() {
            Manager.ContentManager.CurrentVideoStopTime = videoPlayer.time;
            videoPlayer.loopPointReached -= AfterTheEnd;
            videoPlayer.prepareCompleted -= VideoIsPrepared;
        }
        
        public void Init(VideoData videoData) {
            if (videoData == null) {
                // Open popUp with some message (Maybe send error message to backend.....)
                return;
            }
            if (videoData.name == "AR_CHEERS")
            {
                transform.localScale = Vector3.one * 2;
            }
            //Manager.ContentManager.GetOrientation(videoData.orientation);

            if (Manager.ContentManager.IsFrontCameraOn) {
                planeObj.transform.localScale = new Vector3(planeObj.transform.localScale.x * (-1), planeObj.transform.localScale.y, planeObj.transform.localScale.z);
            }

            currentVideoData = videoData;

            if (videoPlayer == null) {
                return;
            }

            videoPlayer.isLooping = videoData.loop;
            videoPlayer.source = VideoSource.Url;
            videoPlayer.url = videoData.GetContentPath();


            videoPlayer.time = Manager.ContentManager.CurrentVideoStopTime;

            if (videoData.timeline?.Length > 0) {
                var effectContr = GetComponent<EffectController>();

                if (effectContr == null) {
                    effectContr = gameObject.AddComponent<EffectController>();
                }

                foreach (var elem in videoData.timeline) {
                    effectContr.Effects.Add(new Effect(elem));
                }

                //effectContr.SpawnEffect();
                effectContr.SpawnAsync();
            }
        }

        public void ClearVideoTexture() {
            videoPlayer.targetTexture.Release();
        }

        private void AfterTheEnd(VideoPlayer vp) {
            OnVideoEndIsReached.Invoke();
            Manager.ContentManager.CurrentVideoStopTime = 0d;
            if (!currentVideoData.loop) {
                Manager.ContentManager.IsVideoContentPlay = false;
            }
        }

        private void VideoIsPrepared(VideoPlayer vp) {

            OnVideoStatusChange.Invoke();
            Manager.ContentManager.IsVideoContentPlay = true;
            if (vp.isPrepared) {
                //Debug.Log("+++++++++++++++++++++++++++++++++ " + videoPlayer.length);
                Manager.ContentManager.CurrentVideoClipLength = System.Math.Round(videoPlayer.length, 2);
            }
        }
    }
}
