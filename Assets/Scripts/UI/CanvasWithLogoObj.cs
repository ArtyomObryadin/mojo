﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class CanvasWithLogoObj : MonoBehaviour {

    [SerializeField]
    Camera arCamera;

    RectTransform rectTrans;
    float fieldOfView = 0f;
    bool isChanged = true;

    private Vector2 _screenSize;
    private Vector2 _logoRectSize;
    private Vector2 _rectTransSize;

    private Vector3 pointH;
    private Vector3 pointHA;

    private Vector3 pointV;
    private Vector3 pointVA;

    private void Awake() {
        fieldOfView = arCamera.fieldOfView;
        rectTrans = GetComponent<RectTransform>();

        _screenSize = Vector2.zero;
    }

    void Update() {

        if (_screenSize.x > Screen.width || _screenSize.x < Screen.width)
        {
            isChanged = true;
        }
        if (_screenSize.y > Screen.height || _screenSize.y < Screen.height)
        {
            isChanged = true;
        }

        if (fieldOfView > arCamera.fieldOfView || fieldOfView < arCamera.fieldOfView)
        {
            fieldOfView = arCamera.fieldOfView;
            isChanged = true;
        }

        if (isChanged) {
            isChanged = !isChanged;
            _screenSize = new Vector2(Screen.width, Screen.height);
            StopAllCoroutines();
            StartCoroutine(Recalculate(0));
        }       
    }

    IEnumerator Recalculate(int frameForWait) {
        for (int a = 0 ; a < frameForWait; a++) {
            yield return new WaitForEndOfFrame();
        }

        rectTrans.sizeDelta = new Vector2(_screenSize.x, _screenSize.y);

        rectTrans.position = arCamera.ScreenToWorldPoint(new Vector3(0.5f * Screen.width, 0.5f * Screen.height, rectTrans.localPosition.z));

        Vector3 pointHorizontal = arCamera.ScreenToWorldPoint(new Vector3(Screen.width, 0.5f* Screen.height, rectTrans.localPosition.z));
        Vector3 pointVertical = arCamera.ScreenToWorldPoint(new Vector3(0.5f* Screen.width, Screen.height, rectTrans.localPosition.z));

        float width = Vector3.Distance(arCamera.transform.position + arCamera.transform.forward * (rectTrans.localPosition.z),
                                                                                                               pointHorizontal) * 2;

        float height = Vector3.Distance(arCamera.transform.position + arCamera.transform.forward * (rectTrans.localPosition.z),
                                                                                                                 pointVertical) * 2;
        rectTrans.localScale = new Vector3(1.0f / _screenSize.x, 1.0f / _screenSize.y, 1) * 1.01f;
        rectTrans.sizeDelta = new Vector2(width * _screenSize.x, height * _screenSize.y)*1.01f;
    }
}
