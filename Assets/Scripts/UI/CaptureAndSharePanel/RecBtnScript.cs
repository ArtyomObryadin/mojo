﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
#pragma warning disable 649

[RequireComponent(typeof(EventTrigger))]
public class RecBtnScript : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
    [SerializeField] private Image fillImage;
    [SerializeField] private Image upImage;
    [SerializeField] private CaptureAndSharePanel captureObj;
    [SerializeField] private float maxRecordTime;
    [SerializeField] private float buttonHoldForRecordVideo;

    float delayTime;
    bool isRecAllow = true;
    bool record = false;
    bool buttonHold = false;
    bool isVideoEx = false;
    float animDelay = 0f;
    float timer;

    void Start() {
        delayTime = Time.time;
    }

    private void OnDisable() {
        if (record) {
            VideoRecordingIsEnd();
            fillImage.fillAmount = 0;
        }
    }

    void Update() {
        if (!buttonHold) {
            if (Time.time - delayTime <= animDelay) {
                isRecAllow = false;
                upImage.color = new Vector4(0.66f, 0.66f, 0.66f, 1f); // light gray color
                return;
            }
            else {
                isRecAllow = true;
                upImage.color = new Vector4(0f, 0f, 0f, 1f); // black color
            }
        }
        else {
            if (record) {
                upImage.color = new Vector4(0.9529f, 0.5882f, 0.4862f, 1f); // orange color
            }
        }

        timer += Time.deltaTime;
        if (buttonHold && !record) {
            if (timer > buttonHoldForRecordVideo) {
                record = true;
                captureObj.StartRecord();
            }
        }

        if (record && isRecAllow) {
            fillImage.fillAmount = timer / maxRecordTime;
            //Debug.Log(timer / maxRecordTime);
            if (timer >= maxRecordTime) {
                isRecAllow = false;
                VideoRecordingIsEnd();
            }
        }
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData) {
        if (isRecAllow) {
            buttonHold = true;
            timer = 0;
            isVideoEx = false;
        }
    }

    void IPointerUpHandler.OnPointerUp(PointerEventData eventData) {
        if (isRecAllow && !isVideoEx) {
            animDelay = captureObj.AnimDelay;
            delayTime = Time.time;
            buttonHold = false;
            if (record) {
                captureObj.StopRecord();
                record = false;
                isRecAllow = false;
            }
            else {
                captureObj.Screenshot();
            }
        }
        fillImage.fillAmount = 0;
    }

    void VideoRecordingIsEnd() {
        captureObj.StopRecord();
        record = false;
        buttonHold = false;
        delayTime = Time.time;
        isVideoEx = true;
    } 
}
