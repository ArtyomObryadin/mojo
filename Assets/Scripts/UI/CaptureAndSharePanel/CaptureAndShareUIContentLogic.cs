﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class CaptureAndShareUIContentLogic : MonoBehaviour {
    [SerializeField] private RawImage ContentImage;
    [SerializeField] private Image WhiteImage;
    [SerializeField] private Vector3 EndCornerPos;
    [SerializeField] private Vector3 EndOutPos;
    [SerializeField] private Vector3 EndSize;
    [SerializeField] private Transform ShareButton;
    [SerializeField] private float timeToCornerAnim;
    [SerializeField] private float timeForWaitAnim;
    [SerializeField] private float timeForOutAnim;
    [SerializeField] private VideoPlayer mainVideoPlayer;
    [SerializeField] private AnimationCurve smoothCurve;

    private string tempFilePath;

    private float animTime;
    private bool isVideo;
    Vector2 scaleCan;

    public float TimeToCornerAnim {
        get => timeToCornerAnim;
        set => timeToCornerAnim = value;
    }

    public float TimeForWaitAnim {
        get => timeForWaitAnim;
        set => timeForWaitAnim = value;
    }

    public float TimeForOutAnim {
        get => timeForOutAnim;
        set => timeForOutAnim = value;
    }

    public void Init (bool _isVideo, string _tempFilePath, Texture2D screenShot) {
        isVideo = _isVideo;
        tempFilePath = _tempFilePath;
        gameObject.SetActive(true);

        if (isVideo) {
            mainVideoPlayer.url = _tempFilePath;
            mainVideoPlayer.Play();
            mainVideoPlayer.renderMode = VideoRenderMode.APIOnly;
        }
        else {
            ContentImage.texture = screenShot;
        }
    }

    private void OnDisable() {
        DeInit();
    }

    void Update() {
        if (animTime > TimeForOutAnim + TimeForWaitAnim + TimeToCornerAnim) {
            DeInit();
            return;
        }

        animTime += Time.deltaTime;

        if (animTime <= TimeToCornerAnim) {
            float t = smoothCurve.Evaluate(animTime / TimeToCornerAnim);
            transform.position = Vector3.Lerp(Vector3.zero, EndCornerPos, t);
            transform.localScale = Vector3.Lerp(Vector3.one, EndSize, t);
            ShareButton.localScale = Vector3.Lerp(Vector3.one, Vector3.one, t);

            WhiteImage.color = new Color(1, 1, 1, 1f - t);
        }

        if (animTime > TimeToCornerAnim + TimeForWaitAnim) {
            float t = smoothCurve.Evaluate((animTime - TimeForWaitAnim - TimeToCornerAnim) / TimeForOutAnim);
            transform.position = Vector3.Lerp(EndCornerPos, EndOutPos, t);
        }
         
        if (isVideo) {
            ContentImage.texture = mainVideoPlayer.texture;
            mainVideoPlayer.SetDirectAudioMute(0, true);
        }
    }

    void DeInit() {
        //DestroyTempFile();
        Destroy(gameObject);
    }

    void DestroyTempFile() {
        if (File.Exists(tempFilePath)) {
            File.Delete(tempFilePath);
        }
    }
}